import ipaddress
import json
import os
import sys
from urllib.parse import urlencode
from urllib.request import Request, urlopen


def find_ipv6_addr():
    # find suitable ipv6 address
    with open('/proc/net/if_inet6', 'r') as f:
        for line in f:
            ip_address, interface_number, prefix, scope, flags, name = \
                line.split()

            ip_address = ipaddress.IPv6Address(int(ip_address, base=16))
            if ip_address.is_global:
                return ip_address


if __name__ == '__main__':
    ipv6_address = find_ipv6_addr()

    with open(os.path.join(os.path.dirname(__file__), 'settings.json')) as f:
        config = json.load(f)

    data = {
        'api_key': config['api_key'],
        'ip': str(ipv6_address),
        'host': config['host'],
    }

    request = Request(config['server'], urlencode(data).encode())
    with urlopen(request) as response:

        print(response.getcode())
        print(response.read().decode())

    if response.getcode() == 200:
        sys.exit(0)
    sys.exit(1)
